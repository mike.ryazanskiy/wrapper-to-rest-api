import requests
import asyncio
import aiohttp


class WrapperApi:
    def __init__(self, api_token):

        self.access_token = api_token
        self.link = Link(self.access_token)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return 0


class Link:
    def recieve_curs(self, currency):
        params = {'access_token': self.token, 'symbols': currency}
        feedback = self.get_connect(self.link, params)
        return Response(feedback)

    def __init__(self, token_):
        self.token = token_
        self.link = "http://data.fixer.io/api/latest"

    def get_connect(self, url, params):
        return requests.get(url=url+'?access_key='+self.token, params=params)


class Response:
    def __init__(self, resp):

        self.curs = resp.json()['rates']


class Error(KeyError):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


class WrapperException(Error):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


if __name__ == "__main__":
    with open('token.txt') as file:
        token = file.read()

    with WrapperApi(token) as api:

        responses = [api.link.recieve_curs('GBP'), api.link.recieve_curs('RUB')]
        for response in responses:
            print(response.curs)
